Reproduce https://github.com/renovatebot/renovate/issues/17432

#### given

- merged result is enabled under Settings > General > Merge requests > Enable merged results pipelines
- automerge is enabled in `renovate.json`
- renovate has already created the merge request for updating node https://gitlab.com/gbleu/renovate-17432-repro/-/merge_requests/3
- merge result pipeline is green

#### when

- renovate job is triggered under https://gitlab.com/gbleu/renovate-17432-repro/-/pipeline_schedules

#### then

- the merge request is not merged
